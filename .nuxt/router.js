import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _22765731 = () => interopDefault(import('..\\pages\\About.vue' /* webpackChunkName: "pages_About" */))
const _07aceeeb = () => interopDefault(import('..\\pages\\Home.vue' /* webpackChunkName: "pages_Home" */))
const _6a87dbd4 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
      path: "/About",
      component: _22765731,
      name: "About"
    }, {
      path: "/Home",
      component: _07aceeeb,
      name: "Home"
    }, {
      path: "/",
      component: _6a87dbd4,
      name: "index"
    }],

  fallback: false
}

export function createRouter() {
  return new Router(routerOptions)
}
